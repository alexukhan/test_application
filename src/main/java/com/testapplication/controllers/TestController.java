package com.testapplication.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {

    @GetMapping("test/ok")
    public ResponseEntity testOk()   {
        String message = "TEST MESSAGE GOOD";
        ResponseEntity entity = new ResponseEntity(message, HttpStatus.OK);
        return entity;
    }

    @GetMapping("test/ok-with-pause")
    public ResponseEntity testOkWithPause()  throws InterruptedException   {
        Thread.sleep(4000);
        ResponseEntity entity = new ResponseEntity(HttpStatus.OK);
        return entity;
    }

    @GetMapping("test/server-error")
    public ResponseEntity testServerError()    {
        String errorMessage = "Server Error";
        ResponseEntity entity = new ResponseEntity(errorMessage, HttpStatus.INTERNAL_SERVER_ERROR);
        return entity;
    }
}
